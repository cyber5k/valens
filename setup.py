
#from distutils.core import setup
#from distutils.extension import Extension
from setuptools import find_packages, setup, Extension

vmodule =  Extension("valens", 
                      sources=["valens.pyx",
                               "util/libntru/src/ntru.c",
                               "util/libntru/src/key.c",
#                                   "libntru/src/encparams.c",
                               "util/libntru/src/hash.c",
                               "util/libntru/src/rand.c",
                               "util/libntru/src/idxgen.c",
                               "util/libntru/src/sha2.c",
                               "util/libntru/src/poly.c",
                               "util/libntru/src/nist_ctr_drbg.c",
                               "util/libntru/src/rijndael.c",
                               "util/libntru/src/bitstring.c",
                               "util/libntru/src/sha1.c",
                               "util/libntru/src/mgf.c",
                               "util/libntru/src/arith.c",
                               "util/aaa.c"],
                        include_dirs=['util/libntru/src/'],
#                            extra_compile_args=['-fgnu89-inline'],
)


ext_modules = [vmodule]

try:
    from Cython.Build import cythonize
    ext_modules = cythonize(ext_modules)
except:
    print("Unable to cythonize")

try:
    import pypandoc
    long_description = pypandoc.convert('README.md', 'rst')
except (IOError, ImportError):
    long_description = open('README.md').read()

setup(
    name="valens",
    version="0.1",
    description="Python wrapper for C implementation of NTRU",
    long_description=long_description,
    url='https://github.com/sfoerster/valens',
    author="Steven Foerster",
    author_email="sfoerster@gmail.com",
    packages=find_packages(),
    setup_requires=[
        'cython>=0.21',
    ],
    install_requires=[],
    test_suite='tests.test_module',
    ext_modules=ext_modules,
    keywords=[
        'NTRU',
        'quantum-resistant',
        'post-quantum',
        'crypto',
        'cryptography',
        'lattice',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
