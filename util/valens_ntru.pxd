
# valens_ntru.pxd

from libc.stdint cimport uint8_t, uint16_t, uint32_t

from valens_ctypes cimport *
from valens_encparams cimport *
from valens_rand cimport *

cdef extern from "libntru/src/ntru.h":

    uint8_t ntruprime_gen_key_pair(const NtruPrimeParams* params, NtruPrimeKeyPair* kp, NtruRandContext* rand_ctx);
    uint8_t ntru_gen_key_pair(const NtruEncParams* params, NtruEncKeyPair* kp, NtruRandContext* rand_ctx);
    uint8_t ntru_gen_key_pair_multi(const NtruEncParams* params, NtruEncPrivKey* priv, NtruEncPubKey* pub, NtruRandContext* rand_ctx, uint32_t num_pub);
    uint8_t ntru_gen_pub(const NtruEncParams* params, NtruEncPrivKey* priv, NtruEncPubKey* pub, NtruRandContext* rand_ctx);
    uint8_t ntru_encrypt(uint8_t* msg, uint16_t msg_len, NtruEncPubKey* pub, const NtruEncParams* params, NtruRandContext* rand_ctx, uint8_t* enc);
    uint8_t ntru_decrypt(uint8_t* enc, NtruEncKeyPair* kp, NtruEncParams* params, uint8_t* dec, uint16_t* dec_len);
    uint8_t ntru_max_msg_len(NtruEncParams* params);
