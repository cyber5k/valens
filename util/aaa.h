#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "types.h"
#include "ntru_endian.h"
#include "ntru.h"
#include "encparams.h"
#include "key.h"
#include "rand.h"
#include "err.h"

struct NtruEncParams sf_get_params_EES1087EP2(void);
struct NtruEncParams sf_get_params_EES1171EP1(void);
struct NtruEncParams sf_get_params_EES1499EP1(void);
struct NtruEncParams sf_get_params_EES743EP1(void);
struct NtruEncParams sf_get_params(void);
NtruEncKeyPair sf_gen_ntru_key(void);
uint8_t sf_ntru_encrypt(uint8_t *msg, NtruEncPubKey *pub, const NtruEncParams *params, uint8_t *enc);
uint8_t sf_ntru_decrypt(uint8_t *enc, NtruEncKeyPair *kp, const NtruEncParams *params, uint8_t *dec, uint16_t *dec_len); 
uint8_t sf_ntru_max_msg_len(const NtruEncParams *params);
