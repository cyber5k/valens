# valens_ctypes.pxd

from libc.stdint cimport uint8_t, uint16_t, int16_t
cimport cpython.array


cdef extern from "util/libntru/src/types.h":
    DEF NTRU_MAX_DEGREE = (1499+1)   #/* max N value for all param sets; +1 for ntru_invert_...() and ntruprime_inv_poly() */
    DEF NTRU_INT_POLY_SIZE = ((NTRU_MAX_DEGREE+16+7)&0xFFF8)   #/* (max #coefficients + 16) rounded to a multiple of 8 */
    DEF NTRU_MAX_ONES = 499   #/* max(df1, df2, df3, dg) */

    #/** A polynomial with 16-bit integer coefficients. */
    ctypedef struct NtruIntPoly:
        uint16_t N
        int16_t coeffs[NTRU_INT_POLY_SIZE]

    #/** A ternary polynomial, i.e. all coefficients are equal to -1, 0, or 1. */
    ctypedef struct NtruTernPoly:
        uint16_t N
        uint16_t num_ones
        uint16_t num_neg_ones
        uint16_t ones[NTRU_MAX_ONES]
        uint16_t neg_ones[NTRU_MAX_ONES]

    #ifndef NTRU_AVOID_HAMMING_WT_PATENT
    
    #/**
    # * A product-form polynomial, i.e. a polynomial of the form f1*f2+f3
    # * where f1,f2,f3 are very sparsely populated ternary polynomials.
    # */
    ctypedef struct NtruProdPoly:
        uint16_t N
        NtruTernPoly f1, f2, f3
    
    #endif   /* NTRU_AVOID_HAMMING_WT_PATENT */

    #/** Private NtruEncrypt polynomial, can be ternary or product-form */
    ctypedef union NtruPrivPoly_union:
        NtruTernPoly tern
        NtruProdPoly prod
    
    ctypedef struct NtruPrivPoly:
        uint8_t prod_flag   #/* whether the polynomial is in product form */
        NtruPrivPoly_union poly

    #/**
    # * NTRU Prime public key
    # */
    ctypedef struct NtruPrimePubKey:
        uint16_t p
        NtruIntPoly h

    #/**
    # * NTRU Prime private key
    # */
    ctypedef struct NtruPrimePrivKey:
        uint16_t p
        NtruIntPoly f
        NtruIntPoly g_inv

    #/**
    # * NTRU Prime key pair
    # */
    ctypedef struct NtruPrimeKeyPair:
        NtruPrimePrivKey priv
        NtruPrimePubKey pub

    #/**
    # * NtruEncrypt private key
    # */
    ctypedef struct NtruEncPrivKey:
        uint16_t q
        NtruPrivPoly t

    #/**
    # * NtruEncrypt public key
    # */
    ctypedef struct NtruEncPubKey:
        uint16_t q
        NtruIntPoly h

    #/**
    # * NtruEncrypt key pair
    # */
    ctypedef struct NtruEncKeyPair:
        NtruEncPrivKey priv
        NtruEncPubKey pub
    #endif   /* NTRU_TYPES_H */
