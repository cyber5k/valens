
####### distutils: sources = util/libntru/src/ntru.c
# distutils: include_dirs = util/libntru/src/
# cython: language_level=3

from libc.stdio cimport printf, setbuf, stdout
from libc.stdint cimport uint8_t, uint16_t, int16_t
from libc.stdlib cimport calloc, free
#from libc.string cimport strcpy
cimport cpython.array

from util cimport valens_ctypes
from util cimport valens_key
from util cimport valens_encparams
from util cimport valens_aaa

from random import randint

cdef class Valens:

    ### (mainly) C variables ###
    cdef valens_encparams.NtruEncParams encparams
    #cdef valens_rand.NtruRandGen* rng_def
    cdef valens_ctypes.NtruEncKeyPair keypair
    cpdef uint8_t* pubkey_export
    cpdef uint8_t* privkey_export
    cpdef uint8_t* cmsg
    cpdef uint8_t* cenc
    
    cpdef uint8_t* dec
    cpdef uint16_t dec_len
    
    cpdef int publen
    cpdef int privlen
    cpdef int max_msg_len
    cpdef int enclen

    paramtypes = [
        'EES1087EP2',
#        'EES1171EP1',
#        'EES1499EP1',
#        'EES743EP1',
    ]

    def __cinit__(self, paramtype=None):
        """
        Initialize the NTRU wrapper.
        """
        #print('Valens is initializing.')
        setbuf(stdout, NULL) # otherwise printf waits for \n to flush buffer

        if not paramtype:
            self.encparams = valens_aaa.sf_get_params()
        elif paramtype.upper() in self.paramtypes:
            if paramtype.upper() == 'EES1087EP2':
                self.encparams = valens_aaa.sf_get_params_EES1087EP2()
            elif paramtype.upper() == 'EES1171EP1':
                self.encparams = valens_aaa.sf_get_params_EES1171EP1()
            elif paramtype.upper() == 'EES1499EP1':
                self.encparams = valens_aaa.sf_get_params_EES1499EP1()
            elif paramtype.upper() == 'EES743EP1':
                self.encparams = valens_aaa.sf_get_params_EES743EP1()
            else:
                raise Exception('Parameters not loaded.')
        else:
            raise Exception('Unrecognized parameter type. Must be in: {}'.format(self.paramtypes))
            
       
        self.publen = valens_key.ntru_pub_len(&self.encparams)
        self.privlen = valens_key.ntru_priv_len(&self.encparams)
        self.enclen = valens_encparams.ntru_enc_len(&self.encparams)
        #self.max_msg_len = 106 # HARD CODED for 256-bit params (don't want to cimport from ntru.h here)
        self.max_msg_len = valens_aaa.sf_ntru_max_msg_len(&self.encparams) 

        self.pubkey_export = <uint8_t *>calloc(self.publen, sizeof(uint8_t))
        self.privkey_export = <uint8_t *>calloc(self.privlen, sizeof(uint8_t))
        self.cmsg = <uint8_t *>calloc(self.max_msg_len, sizeof(uint8_t))
        self.cenc = <uint8_t *>calloc(self.enclen, sizeof(uint8_t))

        self.dec = <uint8_t *>calloc(self.max_msg_len, sizeof(uint8_t))

    def __dealloc__(self):
        """
        Free memory allocation of C stuff.
        """
        #free(<void *>self.rng_def)
        #free(void* self.encparams)
        free(<void *>self.pubkey_export)
        free(<void *>self.privkey_export)
        free(<void *>self.cmsg)
        free(<void *>self.cenc)
        free(<void *>self.dec)

    def __init__(self, paramtype=None):
        """
        Initialize the Python stuff.
        """
        self.keypair = valens_aaa.sf_gen_ntru_key()
        valens_key.ntru_export_pub(&self.keypair.pub, self.pubkey_export)
        valens_key.ntru_export_priv(&self.keypair.priv, self.privkey_export)
        
    def encryptMsg(self, msg):
        """
        Encrypt message with already stored key.
        """
        
        if len(msg) > self.max_msg_len:
            print("Warning: msg is longer than {}. only the first part of the message string will be encrypted.".format(self.max_msg_len))

        # copy message to c buffer
        for i in range(self.max_msg_len):
            if i < len(msg):
                self.cmsg[i] = ord(msg[i])
            elif i == len(msg):
                self.cmsg[i] = 0
            else:
                self.cmsg[i] = randint(0, 255)
        
        # encrypt
        if (valens_aaa.sf_ntru_encrypt(self.cmsg, &self.keypair.pub, &self.encparams, self.cenc) != 0): #valens_err.NTRU_SUCCESS):
            raise Exception("encrypt fail")

        return "".join(["{:02x}".format(self.cenc[i]) for i in range(self.enclen)])

    def decryptMsg(self, msg):
        """
        Decrypt message with already stored key.
        """
        if len(msg) % 2 != 0:
            raise Exception('Valens.decryptMsg: bad msg length') 

        for i in range(self.enclen):
            k = 2*i
            if k < len(msg):
                self.cenc[i] = int.from_bytes(bytes.fromhex(msg[k:k+2]), byteorder='big')
            elif k == len(msg):
                self.cenc[i] = 0
            else:
                self.cenc[i] = randint(0, 255)

        
        if (valens_aaa.sf_ntru_decrypt(self.cenc, &self.keypair, &self.encparams, self.dec, &self.dec_len) != 0): # NTRU_SUCCESS)
            raise Exception('Valens.decryptMsg: decryption failed')

        if str(self.dec_len).isnumeric():
            return "".join([chr(self.dec[i]) for i in range(self.dec_len)])
        else:
            raise Exception('Valens.decryptMsg: something went wrong')

    def importPubKey(self, pubkey):
        """
        Import the public key as a hex string.
        """

        if len(pubkey) != 2*self.publen:
            raise Exception("Public key is wrong size. Should be: {}".format(2*self.publen))

        for i in range(self.publen):
            k = 2*i
            if k < len(pubkey):
                self.pubkey_export[i] = int.from_bytes(bytes.fromhex(pubkey[k:k+2]), byteorder='big')
            elif k == len(pubkey):
                self.pubkey_export[i] = 0
            else:
                raise Exception("importPubKey: Something unexpected happened.")

        retval = valens_key.ntru_import_pub(self.pubkey_export, &self.keypair.pub)
        
        # Fill pubkey_export with contents of keypair
        valens_key.ntru_export_pub(&self.keypair.pub, self.pubkey_export)

        return retval

    def importPrivKey(self, privkey):
        """
        Import the private key as a hex string.
        """

        if len(privkey) != 2*self.privlen:
            raise Exception("Private key is the wrong size. Should be: {}".format(2*self.privlen))

        for i in range(self.privlen):
            k = 2*i
            if k < len(privkey):
                self.privkey_export[i] = int.from_bytes(bytes.fromhex(privkey[k:k+2]), byteorder='big')
            elif k == len(privkey):
                self.privkey_export[i] = 0
            else:
                raise Exception("importPrivKey: something unexpected happened.")
        
        retval = valens_key.ntru_import_priv(self.privkey_export, &self.keypair.priv)

        # Fill privkey_export with contents of keypair
        valens_key.ntru_export_priv(&self.keypair.priv, self.privkey_export)

        return retval

    def getPubKey(self):
        """
        Get public key as a hex string.
        """
        return "".join(["{:02x}".format(<unsigned char>self.pubkey_export[x]) for x in range(self.publen)])

    def getPrivKey(self):
        """
        Get private key as a hex string.
        """
        return "".join(["{:02x}".format(<unsigned char>self.privkey_export[x]) for x in range(self.privlen)])
    
    def printPubKey(self):
        """
        Print the public key 
        """
        
        print("---- BEGIN NTRU PUBLIC KEY -----")
        print(self.getPubKey())
        print("---- END NTRU PUBLIC KEY -----")

    def printPrivKey(self):
        """
        Print the private key
        """

        print("---- BEGIN NTRU PRIVATE KEY -----")
        print(self.getPrivKey())
        print("---- END NTRU PRIVATE KEY -----")

    def get_max_msg_len(self):
        """
        Get the maximum length of a message that can be encrypted.
        """
        return self.max_msg_len
