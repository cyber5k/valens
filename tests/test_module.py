import unittest
import valens
    
import string
from random import choice, randint

def gen_string(min_char=8, max_char=64):
    """
    Generate a random string.
    """
    allchar = string.ascii_letters + string.punctuation + string.digits
    return "".join(choice(allchar) for x in range(randint(min_char, max_char)))

class ValensTestCase(unittest.TestCase):
    """
    Tests for Valens.
    """
    
    def setUp(self):
        """
        Setting environment for Valens.
        """
        self.V = valens.Valens()
        self.V2 = valens.Valens()

    def test_encrypt_decrypt(self):
        """
        Generating NTRU keypair.
        """
        msg = gen_string(min_char=90, max_char=100)
        ciph = self.V.encryptMsg(msg)
        pln = self.V.decryptMsg(ciph)
        self.assertEqual(msg, pln)

    def test_public_key_import(self):
        """
        Import public key.
        """
        pubkey2 = self.V2.getPubKey()
        self.V.importPubKey(pubkey2)
        
        pubkey = self.V.getPubKey()
        #print(pubkey)
        #print(pubkey2)
        self.assertEqual(pubkey, pubkey2)

    def test_private_key_import(self):
        """
        Import private key.
        """

        privkey2 = self.V2.getPrivKey()
        self.V.importPrivKey(privkey2)

        privkey = self.V.getPrivKey()
        self.assertEqual(privkey, privkey2)

    def test_public_key_import_encrypt(self):
        """
        Import public key then encrypt. Test decryption.
        """

        pubkey2 = self.V2.getPubKey()
        self.V.importPubKey(pubkey2)

        msg = gen_string(min_char=90, max_char=100)

        ciph = self.V.encryptMsg(msg)
        pln = self.V2.decryptMsg(ciph)

        self.assertEqual(msg, pln)

    def test_private_key_import_encrypt(self):
        """
        Encrypt message. Then import public and private keys to another insance and decrypt.
        """

        msg = gen_string(min_char=90, max_char=100)
        ciph = self.V.encryptMsg(msg)

        self.V2.importPubKey(self.V.getPubKey())
        self.V2.importPrivKey(self.V.getPrivKey())

        pln = self.V2.decryptMsg(ciph)
        self.assertEqual(msg, pln)

    def test_wrong_key_decrypt(self):
        """
        Attempt to decrypt message with wrong key.
        """
        msg = gen_string(min_char=90, max_char=100)
        ciph = self.V.encryptMsg(msg)
        with self.assertRaises(Exception):
            self.V2.decryptMsg(msg)

    def test_max_msg_len(self):
        """
        Get the maximum message length.
        """
        V3 = valens.Valens(paramtype='EES1087EP2')
        self.assertEqual(V3.get_max_msg_len(), 170) # hardcoded for EES1087EP2 params

    def test_params_EES1087EP2(self):
        """
        Test the EES1087EP2 parameters.
        """
        V3 = valens.Valens(paramtype='EES1087EP2')
        msg = gen_string(min_char=90, max_char=100)
        ciph = V3.encryptMsg(msg)
        pln = V3.decryptMsg(ciph)
        self.assertEqual(msg, pln)

    #def test_params_EES1499EP1(self):
    #    """
    #    Test the EES1499EP1 parameters.
    #    """
    #    V3 = valens.Valens(paramtype='EES1499EP1')
    #    msg = gen_string(min_char=90, max_char=100)
    #    ciph = V3.encryptMsg(msg)
    #    pln = V3.decryptMsg(ciph)
    #    self.assertEqual(msg, pln)
    #
    #def test_params_EES1171EP1(self):
    #    """
    #    Test the EES1171EP1 parameters.
    #    """
    #    V3 = valens.Valens(paramtype='EES1171EP1')
    #    msg = gen_string(min_char=90, max_char=100)
    #    ciph = V3.encryptMsg(msg)
    #    pln = V3.decryptMsg(ciph)
    #    self.assertEqual(msg, pln)
    #
    #def test_params_EES743EP1(self):
    #    """
    #    Test the EES743EP1 parameters.
    #    """
    #    V3 = valens.Valens(paramtype='EES743EP1')
    #    msg = gen_string(min_char=90, max_char=100)
    #    ciph = V3.encryptMsg(msg)
    #    pln = V3.decryptMsg(ciph)
    #    self.assertEqual(msg, pln)

if __name__ == '__main__':
    unittest.main()

